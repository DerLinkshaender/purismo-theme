
# Konfiguration und Anpassung des Themes

## Allgemeines

Die Konfiguration eines Hugo-Blogs erfolgt normalerweise über die Konfigurationsdatei `config.toml` im Basisordner der Hugo-Installation.
Dabei handelt es sich um eine Textdatei im [TOML-Format](https://github.com/toml-lang/toml), diese kann also mit einem Editor für Textdateien
bearbeitet werden.

Der Aufbau entspricht grob folgendem Schema:

* Grundeinstellungen
* Theme-spezifische Einstellungen im Abschnitt `[params]`
* Definition eventueller Menüstrukturen oder fester Seiten


## Grundeinstellungen


	baseURL = "http://example.com/"
	languageCode = "de-DE"
	language = "de-DE"
	defaultContentLanguage = "de-DE"
	title = "Katzen hüten"
	theme = "purismo"
	enableEmoji = true (default is false)



## Theme-spezifische Einstellungen

Die folgenden Einstellungen finden Sie unter dem Abschnitt `[params]` in der Datei `config.toml`.
Falls Sie eine Einstellung nicht benötigen, können Sie diese entweder entfernen oder aber mit einem `#` als erstem
Zeichen in der Zeile als Kommentar markieren (im Beispiel unten beim Eintrag `css` zu sehen).

    author = "Vorname Nachname"
    description = "Ein Blog über die Mittagsbetreuung"
    accent = "green" # or a #rrggbb value
    showBorder = true
    backgroundColor = "white"
    font = "Merriweather"
    #css = ["/css/hurga.css", "/css/burga.css", "/css/akawurga.css"]
    copyright = "Lizenz für Inhalte <a href=\"http://creativecommons.org/licenses/by/4.0/\">CC-BY-SA 4.0</a> Lizenz"
    js_header =
    js_footer = 
    profilepic = "/img/avatar.jpg"   # liegt dann unter /static/img/ im Theme-Ordner
    favicon = 
    


## Menü und Icons in Navigationszeile

In der Konfiguration-Datei `config.toml` kann auch ein Navigationsmenü definiert werden, welches bei
genug Platz in der linken Hälfte der Navigationszeile angezeigt wird.

![Das Navigations- und Icon-Menü](./images/navmenu.png)

### Definition des Menüs

Die Definition erfolgt als "[TOML Array of tables](https://github.com/toml-lang/toml#user-content-array-of-tables)",
wie das folgende Beispiel zeigt. Die Sortierung erfolgt anhand des Tabellenfeldes `weight`, so dass die Reihenfolge
in der `config.toml` nicht relevant ist.


	[[menu.main]]
	    url = "/"
	    name = "Home"
	    weight = 1

	[[menu.main]]
	    url = "/page/dse/"
	    name = "Datenschutz"
	    weight = 2


### Definition des Icon-Bereichs

Analog dazu kann eine Definition von Icons im rechten Bereich der Naviationszeile erfolgen. Der einzige Unterschied zum Menü
ist der Inhalt des Tabellenfeldes `name`. Hier muss ein gültiger Name eines Icons aus den ["Fontawesome"-Icons](https://fontawesome.com/v4.7.0/icons/) enthalten. 

	[[menu.icon]]
	    url = "mailto:me@example.com"
	    name = "envelope-o"
	    weight = 1
	
	[[menu.icon]]
	    url = "https://github.com/username/"
	    name = "github"
	    weight = 2

### Responsive Design

Falls der Platz für die Darstellung nicht ausreicht, wird Menü- und Icon-Bereich zum üblichen "Hamburger-Menü" zusammengeklappt und rechts
oben platziert. Ein Klick auf den Menü-Button klappt dann das Menü nach unten aus.

![Nav-Menü auf mobilen Geräten](./images/navmenu-hamburger.png)


## Fonts

### Lokale Schriftdateien

Im Zuge der DSGVO (engl. GDPR) gab es eine Diskussion, wie einfach und rechtlich sicher die Nutzung der Webfonts-API von Google 
ist. Für das "Purismo"-Theme habe ich daher entschieden, auf lokal gehostete Schrfitarten zu setzen. Wie Sie diese Schriftarten
ändern können, finden Sie weiter unten. Dazu sollten Sie wissen, wie Sie ZIP-Dateien entpacken, Textdateien (die CSS-Dateien) 
ändern und Text aus dem Browser in die Zwischenablage kopieren.

### Konfiguration der Schriftart

Dieses Thema nutzt die Schriftart "Raleway" oder "Merriweather" aus den Google WebFonts, aber ohne diese von den Google-Servern zu laden.
Die Schriftart-Dateien liegen lokal vor. Um eine andere Schriftart zu verwenden, müssen die CSS-Dateien angepasst werden.
Zur Definition der Schriftart wird in der `config.toml` die gewünschte Schriftart im Schlüssel `font` in den Grundeinstellungen angegeben.

    font = "Merriweather"

### Zusätzliche / andere Schriftarten hinzufügen

Über das Tool "[Google Webfonts Helper](https://google-webfonts-helper.herokuapp.com/fonts)" lassen sich die gewünschten
Schriftarten als ZIP-Datei lokal auf den Rechner herunterladen. Zusätzlich muss die Datei `fonts.css` im Theme aktualisiert
werden. Diese liegt im Ordner `purismo-theme/static/css` (alles unterhalb des Ordners `themes` von Hugo).

# Weitere Einstellungen

## Front Matter

Bei einem Post können Sie Metadaten zum Artikel definieren (das sogenannte "_front matter_"). Diese Daten werden
je nach gewähltem [Front Matter Format](https://gohugo.io/content-management/front-matter/) in unterschiedliche
Zeichen eingeschlossen. Für dieses Beispiel verwenden wir TOM (damit dann `+++`):

    +++
    date = "2012-04-06"
    title = "Hugo Themes schreiben in nur einem Jahrzehnt"
    description = "Eine kleine Einführung in 150.000 Wörtern in das Theming von Hugo :-)"
    author = "Ihr netter Autor"
    +++
    
Eine Liste der möglichen Standard-Schlüsselwörter finden Sie [hier](https://gohugo.io/content-management/front-matter/).

Zusätzlich können Sie für das "Purismo"-Theme noch folgende Schlüsselwörter verwenden:

* showinpages: true | false
  * gibt an, ob die Seite in der Liste der Seiten bzw. Posts auftauchen soll oder nicht
* noindex: true | false
  *  definiert, ob das Dokument in einem Suchmaschinenindex auftauchen soll oder nicht


## Archetypes

Hier liegen die "Schablonen" für neue Dokumente, die Sie mit dem Kommando `hugo new ITEM` erzeugen können.
Sie können aber auch einfach mit einer leeren Textdatei starten und Dinge wie _front matter_ selbst tippen.

## Configuration

	baseURL = "http://example.com/"
	languageCode = "de-DE"
	language = "de-DE"
	defaultContentLanguage = "de-DE"
	title = "Katzen hüten"
	theme = "purismo"


	[params]
	    author = "Vorname Nachname"
	    profilepic = "/img/avatar.jpg"
	    description = "Ein Blog über die Mittagsbetreuung"
	    githubUsername = ""
	    accent = "green"
	    showBorder = true
	    backgroundColor = "white"
	    font = "Raleway"
	    #font = "Merriweather"
	    # css = ["/css/hurga.css", "/css/burga.css", "/css/akawurga.css"]
	    copyright = "Lizenz <a href=\"http://creativecommons.org/licenses/by/4.0/\">CC-BY-SA 4.0</a> Lizenz"

	[[menu.main]]
	    url = "/"
	    name = "Home"
	    weight = 1

	[[menu.main]]
	    url = "/page/dse/"
	    name = "Datenschutz"
	    weight = 2

	[[menu.main]]
	    url = "/page/impressum/"
	    name = "Impressum"
	    weight = 3

	[[menu.main]]
	    url = "/post/"
	    name = "Blog-Posts"
	    weight = 4

	[[menu.main]]
	    url = "/page/"
	    name = "Seiten"
	    weight = 5

	# Social icons to be shown on the right-hand side of the navigation bar
	# The "name" field should match the name of the icon to be used
	# The list of available icons can be found at http://fontawesome.io/icons/

	[[menu.icon]]
	    url = "/search"
	    name = "search"
	    weight = 1

	[[menu.icon]]
	    url = "mailto:me@example.com"
	    name = "envelope-o"
	    weight = 2

	[[menu.icon]]
	    url = "https://github.com/username/"
	    name = "github"
	    weight = 3

	[[menu.icon]]
	    url = "https://twitter.com/username/"
	    name = "twitter"
	    weight = 4

	[[menu.icon]]
	    url = "https://www.linkedin.com/in/username/"
	    name = "linkedin"
	    weight = 5

