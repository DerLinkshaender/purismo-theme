

# Purismo

Ein Theme für [Hugo](https://gohugo.io) mit dem Schwerpunkt auf Minimalismus und DSGVO-gerechtes Bloggen.
Keine Cookies, möglichst keine externen Datenquellen, alle Abhängigkeiten lokal gehostet.
Responsive Design, eine Suche, die nocht von externen Scripts oder Quellen abhängt.
Inspiriert vom ["Minimal"-Theme](https://themes.gohugo.io/minimal/) von [Calin Tataru](https://calintat.github.io/)

![Schön auf Desktop, Tablet und Smartphone](./images/screenshot.png)

## Installation

Für die Installation kopierst Du den Ordner mit dem Thema in den Ordner `themes` unterhalb Deiner Hugo-Installation.
Dazu entweder die Funktion für den ZIP-Download aus dem [Gitlab-Repository](https://gitlab.com/DerLinkshaender/purismo-theme)
benutzen. Die ZIP-Datei in einen Ordner `purismo` unterhalb des Ordners `themes` in Deiner Hugo-Installation auspacken.

Alternativ kannst Du auf der Kommandozeile das Repository clonen (dazu in den Ordner `themes` unterhalb des Hugo-Ordners wechseln)
und das folgende Kommando eingeben:

	git clone https://gitlab.com/DerLinkshaender/purismo-theme.git

## Konfiguration

Wenn Du bereits eine Datei `config.toml` für Deine Hugo-Installation hast, solltest Du die Dokumentation durchlesen,
die Du in der Datei [CONFIG.md](./CONFIG.md) findest. Wenn Du gerade ein neues Blog aufgesetzt hast, kannst Du die 
Datei `config.toml.sample` in Deinen Hugo-Basisordner kopieren und in `config.toml` umbenennen und darin die Einstellungen
anpassen.



